const mongoose = require ('mongoose');
const { Schema } = mongoose;

const Customer = new Schema({
    name: String,
    email: String
});

module.exports = mongoose.model('Customer', Customer);