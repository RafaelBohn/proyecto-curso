const mongoose = require ('mongoose');
const { Schema } = mongoose;

const Order = new Schema({
    nombre: String,
    email: String,
    celular: Number,
    producto: String,
    ancho: Number,
    alto: Number,
    cantidad: Number,
    fecha: Date
});

module.exports = mongoose.model('Order', Order);