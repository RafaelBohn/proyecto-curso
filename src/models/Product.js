const mongoose = require ('mongoose');
const { Schema } = mongoose;

const Product = new Schema({
    nombreProducto: String,
    precioProducto: Number
});

module.exports = mongoose.model('Product', Product);