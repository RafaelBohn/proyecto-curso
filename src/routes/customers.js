const express = require('express');
const router = express.Router();

const Customer = require('../models/Customer');

router.get('/', async (req, res) => {
    const customers = await Customer.find();
    res.json(customers);
});

router.post('/', async (req, res) => {
    const customer = new Customer(req.body);
    await customer.save();
    res.json({
        status: 'Customer Saved'
    });
});

router.put('/:id', async (req, res) => {
    await Customer.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Customer Update'
    });
});

router.delete('/:id', async (req, res) => {
    await Customer.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Customer Deleted'
    });
});


module.exports = router;