const express = require('express');
const router = express.Router();

const Order = require('../models/Order');

router.get('/', async (req, res) => {
    const orders = await Order.find();
    res.json(orders);
});

router.post('/', async (req, res) => {
    const order = new Order(req.body);
    await order.save();
    res.json({
        status: 'Order Saved'
    });
});

router.put('/:id', async (req, res) => {
    await Order.findByIdAndUpdate(req.params.id, req.body);
    res.json({
        status: 'Order Update'
    });
});

router.delete('/:id', async (req, res) => {
    await Order.findByIdAndRemove(req.params.id);
    res.json({
        status: 'Order Deleted'
    });
});


module.exports = router;