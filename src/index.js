const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const app = express();
const db = require('../config/keys').mongoURI;
mongoose.connect(db, {useNewUrlParser: true, useUnifiedTopology: true } )
    .then(db => console.log('DB is connected'))
    .catch(err => console.log(err))
;

// Settings
app.set('port', process.env.PORT || 3000);


// Middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Use the passport Middleware
app.use(passport.initialize());
// Bring in the Passport Strategy
require('../config/passport')(passport);

//Routes
app.use('/api/customers', require('./routes/customers'));
app.use('/api/orders', require('./routes/orders'));
app.use('/api/users', require('./routes/users'));
app.use('/api/products', require('./routes/products'));

// Static files
const path = require('path');
//console.log(path.join(__dirname, '..', 'public'))
app.use(express.static(path.join(__dirname, '..', 'public')));

// Server is listening
app.listen(3000, () => {
    console.log('Server on port', app.get('port'));
});