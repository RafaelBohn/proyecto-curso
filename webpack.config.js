const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    externals: {
        express: "require(\"express\")"
    },

    entry: './src/main.js',
    output: {
        path: __dirname + '/public/js',
        filename: 'bundle.js'
        
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader:'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                      },
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',

            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader",
            
            },
            {
                test: /\.(png|jpg|gif)$/i,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                      limit: 8192,
                      esModule: false,
                    },
                  },
                ],
            },
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};
